<?php

use App\Http\Controllers\CalendarController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('home', [HomeController::class, 'index']);

Auth::routes();

Route::group(['middleware' => 'auth:web', 'prefix' => 'calendar'], function () {
    Route::get('/show', [CalendarController::class, 'calendar'])->name('calendar.show');
    Route::get('/events', [CalendarController::class, 'getEvents'])->name('calendar.events');
    Route::post('/createEvent', [CalendarController::class, 'createEvent'])->name('calendar.create');
});
