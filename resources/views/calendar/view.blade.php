@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="calendar"></div>
        <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ trans('calendar.popup.title') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="errors"></div>
                        <label>Időtartam</label>
                        <div class="form-group">
                            <label for="startTime">{{ trans('calendar.from') }}</label>
                            <input class="form-control" readonly id="startTime" name="startTime">
                        </div>
                        <div class="form-group">
                            <label for="endTime">{{ trans('calendar.to') }}</label>
                            <input class="form-control" readonly id="endTime" name="endTime">
                        </div>
                        <div class="form-group">
                            <label for="name">{{ trans('calendar.patient_name') }}</label>
                            <input class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{{ trans('calendar.close') }}</button>
                        <button type="button" id="saveButton" class="btn btn-primary">{{ trans('calendar.save') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
