<?php

return [
    'from' => 'tól',
    'to' => 'ig',
    'patient_name' => 'Páciens neve',
    'close' => 'Bezárás',
    'save' => 'Mentés',
    'popup.title' => 'Új foglalás rögzítése',
    'reserved_period' => 'A választott időpontban már van egy foglalás!',
    'no_office_period' => 'A választott időpont nem rendelési időbe esik!',
];
