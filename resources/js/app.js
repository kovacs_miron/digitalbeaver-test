require('./bootstrap');
window.moment = require('moment');

import {Calendar} from '@fullcalendar/core';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import rrulePlugin from '@fullcalendar/rrule';
import momentPlugin from '@fullcalendar/moment';
import interactionPlugin from '@fullcalendar/interaction'
import allLocales from '@fullcalendar/core/locales-all';


$(document).ready(function () {
    if ($('#calendar').length > 0) {
        let calendarDiv = document.getElementById('calendar');
        let calendar = new Calendar(calendarDiv, {
            plugins: [dayGridPlugin, timeGridPlugin, listPlugin, rrulePlugin, momentPlugin, bootstrapPlugin, interactionPlugin],
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            themeSystem: 'boostrap',
            bootstrapFontAwesome: false,
            navLinks: true,
            editable: true,
            dayMaxEvents: true,
            locales: allLocales,
            locale: 'hu',
            timeZone: 'local',
            initialView: 'timeGridWeek',
            events: {
                url: '/calendar/events',
                method: 'GET',
            },
            selectable: true,
            select: function (data) {
                const startTime = window.moment(data.startStr)
                const endTime = window.moment(data.endStr)
                $('#startTime').val(startTime.format('YYYY-MM-DD HH:mm'))
                $('#endTime').val(endTime.format('YYYY-MM-DD HH:mm'))
                $('#createModal').modal()
            }
        })

        calendar.render()
    }
})

$("#saveButton").click(function () {
    axios.post('/calendar/createEvent', {
        start_time: $("#startTime").val(),
        end_time: $("#endTime").val(),
        name: $("#name").val(),
    })
        .then(function (response) {
            window.location.reload()
        })
        .catch(function (error) {
            renderErrors('errors', error.response.data.errors ? error.response.data.errors : [error.response.data.message])
        })
})

function renderErrors(errorDOM, errors) {
    $(`#${errorDOM}`).empty()
    $.each(errors, (i, error) => {
        $(`#${errorDOM}`).append(`
                    <div>
                        <p class="text-danger">${error}</p>
                    </div>`
        )
    })
}
