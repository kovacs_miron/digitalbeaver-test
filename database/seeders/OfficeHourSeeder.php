<?php

namespace Database\Seeders;

use App\Models\OfficeHour;
use Illuminate\Database\Seeder;

class OfficeHourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'start_time' => '08:00',
                'end_time' => '10:00',
                'start_date' => '2021-07-08',
            ],
            [
                'start_time' => '12:00',
                'end_time' => '16:00',
                'start_date' => '2021-01-01',
                'type' => 'odd-week',
                'day' => 3,
            ],
            [
                'start_time' => '10:00',
                'end_time' => '12:00',
                'start_date' => '2021-01-01',
                'type' => 'even-week',
                'day' => 1,
            ],
            [
                'start_time' => '10:00',
                'end_time' => '16:00',
                'start_date' => '2021-01-01',
                'type' => 'weekly',
                'day' => 5,
            ],
            [
                'start_time' => '16:00',
                'end_time' => '20:00',
                'start_date' => '2021-06-01',
                'end_date' => '2021-11-30',
                'type' => 'weekly',
                'day' => 4,
            ],
        ];

        foreach ($data as $item) {
            OfficeHour::create($item);
        }
    }
}
