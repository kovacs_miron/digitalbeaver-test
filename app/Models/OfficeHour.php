<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OfficeHour
 *
 * @property int $id
 * @property string $start_time
 * @property string $end_time
 * @property string $start_date
 * @property string|null $end_date
 * @property string|null $type null: no repeat, weekly /even-week / odd-week
 * @property int|null $day
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour query()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeHour whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OfficeHour extends Model
{
    protected $table = 'office_hours';

    protected $fillable = [
        'start_time',
        'end_time',
        'start_date',
        'end_date',
        'type',
        'day',
    ];
}
