<?php

namespace App\Http\Controllers;

use App\Services\CalendarService;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * Load calendar view
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function calendar()
    {
        return view('calendar.view');
    }

    /**
     * Get background and normal events
     *
     * @param CalendarService $calendarService
     * @param Request $request
     * @return JsonResponse
     */
    public function getEvents(CalendarService $calendarService, Request $request): JsonResponse
    {
        $end = Carbon::parse($request->end);
        $backgroundEvents = $calendarService->getBackgroundEvents($end);
        $start = Carbon::parse($request->start);
        $events = $calendarService->getEvents($start, $end);

        return new JsonResponse(array_merge($events, $backgroundEvents));
    }

    /**
     * Create new event
     *
     * @param CalendarService $calendarService
     * @param Request $request
     * @return JsonResponse
     */
    public function createEvent(CalendarService $calendarService, Request $request): JsonResponse
    {
        $request->validate([
            'start_time' => 'required|date_format:Y-m-d H:i',
            'end_time' => 'required|date_format:Y-m-d H:i',
            'name' => 'required|string|max:120',
        ]);

        $data = collect($request->all());
        $event = $calendarService->createEvent($data);

        return response()->json(['event' => $event]);
    }
}
