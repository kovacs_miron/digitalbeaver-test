<?php

namespace App\Services;

use App\Models\Event;
use App\Models\OfficeHour;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class CalendarService
{
    /**
     * Create new event
     *
     * @param Collection $data
     * @return Event
     */
    public function createEvent(Collection $data): Event
    {
        $startTime = Carbon::parse($data->get('start_time'));
        $endTime = Carbon::parse($data->get('end_time'));

        $this->validateEvent($startTime, $endTime);

        return Event::create([
            'start_time' => $data->get('start_time'),
            'end_time' => $data->get('end_time'),
            'patient_name' => $data->get('name'),
        ]);
    }

    /**
     * Validate event before create
     *
     * @param Carbon $startTime
     * @param Carbon $endTime
     */
    protected function validateEvent(Carbon $startTime, Carbon $endTime)
    {
        $isReservedPeriod = false;
        if (Event::where('end_time', '>', $startTime)->where('end_time', '<', $endTime)->exists()) {
            $isReservedPeriod = true;
        } elseif (Event::where('start_time', '<', $startTime)->where('end_time', '>', $endTime)->exists()) {
            $isReservedPeriod = true;
        } elseif (Event::where('start_time', $startTime)->exists()) {
            $isReservedPeriod = true;
        } elseif (Event::where('end_time', $endTime)->exists()) {
            $isReservedPeriod = true;
        }

        if ($isReservedPeriod) {
            abort(422, trans('calendar.reserved_period'));
        }

        $backgroundEvents = $this->getBackgroundEvents($endTime);

        $isOfficeHourPeriod = false;
        foreach ($backgroundEvents as $backgroundEvent) {
            if (!$isOfficeHourPeriod && $backgroundEvent['end'] > $startTime && $backgroundEvent['end'] < $endTime) {
                $isOfficeHourPeriod = true;
            } elseif (!$isOfficeHourPeriod && $backgroundEvent['start'] < $startTime && $backgroundEvent['end'] > $endTime) {
                $isOfficeHourPeriod = true;
            } elseif (!$isOfficeHourPeriod && $backgroundEvent['start'] == $startTime) {
                $isOfficeHourPeriod = true;
            } elseif (!$isOfficeHourPeriod && $backgroundEvent['end'] == $endTime) {
                $isOfficeHourPeriod = true;
            }
        }

        if (!$isOfficeHourPeriod) {
            abort(422, trans('calendar.no_office_period'));
        }
    }

    /**
     * Get normal events
     *
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    public function getEvents(Carbon $start, Carbon $end): array
    {
        return Event::whereBetween('start_time', [$start, $end])
            ->get()
            ->map(function (Event $event) {
                return [
                    'start' => $event->start_time,
                    'end' => $event->end_time,
                    'title' => $event->patient_name
                ];
            })
            ->all();
    }

    /**
     * Get background events
     *
     * @param Carbon $end
     * @return array
     */
    public function getBackgroundEvents(Carbon $end): array
    {
        $backgroundDates = [];

        $officeHours = OfficeHour::all();

        foreach ($officeHours as $officeHour) {
            if (!$officeHour->type) {
                $backgroundDates[] = [
                    'start' => Carbon::parse($officeHour->start_date)->setTimeFromTimeString($officeHour->start_time)->toDateTimeString(),
                    'end' => Carbon::parse($officeHour->start_date)->setTimeFromTimeString($officeHour->end_time)->toDateTimeString(),
                ];
            } else {
                $startDate = Carbon::parse($officeHour->start_date)->next($officeHour->day);
                $endDate = Carbon::parse($officeHour->end_date ?: $end);

                for ($date = $startDate; $date->lte($endDate); $date->addWeek()) {
                    $isEvenWeek = $date->week % 2 === 0;
                    if (($officeHour->type === 'odd-week' && !$isEvenWeek) ||
                        ($officeHour->type === 'even-week' && $isEvenWeek) ||
                        $officeHour->type === 'weekly') {
                        $backgroundDates[] = [
                            'start' => Carbon::parse($date)->setTimeFromTimeString($officeHour->start_time)->toDateTimeString(),
                            'end' => Carbon::parse($date)->setTimeFromTimeString($officeHour->end_time)->toDateTimeString(),
                            'display' => 'background',
                        ];
                    }
                }
            }
        }

        // could be put down to cache with date range cache key, e.g redis
        return $backgroundDates;
    }
}
