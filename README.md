# digitalbeaver-test

A feladat laravel 8.x és php7.4 alatt készült, mariadb-vel.

A megfelelő .env konfiguráció és composer csomagok telepítése után elindítható sima php artisan serve parancsal is, nem igényel semmi extra php extensiont.

**Migráció:**

`php artisan migrate`

**Frontendhez szükséges csomagok telepítése és buildelése:**

`npm install && npm run dev`

**Adatbázis feltöltése teszt adatokkal (a feladatleírásban kért adatok):**

`php artisan db:seed`


**Teszt felhasználói fiók:**

test@example.com / 123456